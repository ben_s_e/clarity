#!/usr/bin/env python3

# Clarity -- Common functions
#
# Ben Shirt-Ediss, 2018, 2021
#

import settings

import os
import sys
import pickle
import subprocess





#
#
# Misc
#
#


def shell(cmd) :

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()	




def red_text(msg) :
	print("\033[31m%s\033[0m" % (msg))




def error_msg(msg) :	
	print("")
	red_text(msg)
	print("")
	quit()




def yes_no_confirm() :
	# adapted from: https://stackoverflow.com/questions/3041986/apt-command-line-interface-like-yes-no-input

	confirm = -1
	while confirm == -1 :
		print("Respond by typing \"yes\" or \"no\":")
		
		choice = input().lower()
		if choice == "yes" :
			confirm = True
		elif choice in {'no','n'} :
			confirm = False

	return confirm




def get_commandline_args(min_id) :
	"""
	Returns number of command line arguments specified, 
	and the first and second arguments
	"""

	id = -1
	n = ""

	# catches case where first arg is not a number (cannot be cast to integer)
	try :
		id = int(sys.argv[1])
	except :
		id = -1
	
	# catches case where first arg below the integer value specified
	if id < min_id :
		id = -1

	if len(sys.argv) == 3 :		
		try :
			n = str(sys.argv[2])
		except :
			n = ""

	return len(sys.argv)-1, id, n




def fixedwidth(mystr, n) :
	if len(mystr) >= n :
		return mystr

	while len(mystr) < n :
		mystr = mystr + " "
	return mystr




















#
#
# Desktop slot operations
#
#


def desktop_slot_list_all() :
	"""
	Produce a list of all desktops in the system, and which machines
	have checked out which desktops
	"""

	cyan_colour = "\033[1;36;40m"			# bold
	grey_colour = "\033[3;90;40m"			# italic
	yellow_colour = "\033[1;33;40m"			# bold
	red_colour = "\033[1;31;40m"			# bold
	white_colour = "\033[0;47;40m"

	print("")

	print("%sClarity Desktops%s\n" % (cyan_colour, white_colour))

	ed_left = "     "
	if Desktop_is_empty() :
		ed_left = "%s>>>> %s" % (cyan_colour, white_colour)	
	
	print("%s%s%s" % (ed_left, fixedwidth("0", 5), grey_colour + fixedwidth("Empty Desktop", 35) + white_colour ))

	# list desktops in database
	fname = "%s/_clarity/db.pkl" % (settings.APP_PATH)
	
	if os.path.isfile(fname) :  # check if database exists
		
		f = open(fname, "rb")
		db = pickle.load(f)
		f.close()

		for id in sorted(db["name"].keys()) :
			
			# does folder for this desktop slot still exist?
			# (because it could have been manually moved or deleted via Finder)
			if desktop_slot_exists(id) :

				d_left = "     "
				text_colour = yellow_colour

				# 1. check if this desktop is the one checked out on this computer
				if id == Desktop_get_id() :
					d_left = "%s>>>> %s" % (cyan_colour, white_colour)

				# 2. check if this desktop is checked out by some other unknown computer
				#	(.checkedout file is present in the slot)
				elif desktop_slot_is_checked_out(id) :
					text_colour = red_colour # this desktop is checked out by another computer
				
				# 3. otherwise, no computer is currently displaying this desktop -- it can be checked out

				print("%s%s%s" % (d_left, fixedwidth(str(id), 5), text_colour + fixedwidth(db["name"][id], 35) + white_colour ))

	# list unassigned desktop
	ua_left = "     "
	if Desktop_is_waiting_assignment() :
		ua_left = "%s>>>> %s" % (cyan_colour, white_colour)	

	print("%s%s%s" % (ua_left, fixedwidth("*", 5), grey_colour + fixedwidth("New Desktop (waiting to be assigned)", 35) + white_colour ))

	print("")





def desktop_slot_get_name(id) :
	"""
	Returns the name of the desktop with the passed id number
	If the id number does not exist in the database, or if the database does not exist, then -1 is returned
	(used when deleting desktops)
	"""

	fname = "%s/_clarity/db.pkl" % (settings.APP_PATH)
	if os.path.isfile(fname) :
		
		f = open(fname, "rb")
		db = pickle.load(f)
		f.close()		

		return db["name"].get(id, -1)

	else :
		return -1





def desktop_slot_exists(id) :
	"""
	Returns True if a certain desktop slot with id exists within Clarity
	Note: this is a FOLDER check, not a DATABASE check
	If there is no folder, there is no slot, regardless of what the database says
	"""

	slotdir = "%s/%d" % (settings.APP_PATH, id)

	if os.path.isdir(slotdir) :
		# the numbered folder exists, but also 
		# a .desktopid or .checkedout file must exist within this folder,
		# or else, its just a random numbered folder
		
		if os.path.isfile("%s/.desktopid" % (slotdir)) or \
			os.path.isfile("%s/.checkedout" % (slotdir)) :

			return True

	return False




def desktop_slot_init(id, name) :
	"""
	Sets up a new slot, for a desktop with id and name
	"""

	# make hidden file Desktop/.desktopid, to label this desktop with a number
	Desktop_set_id(id)

	# make slot folder
	shell("mkdir %s/%d" % (settings.APP_PATH, id))

	# put a "checked out" flag in the slot -- so another computer cannot check out this slot
	fname = "%s/%d/.checkedout" % (settings.APP_PATH, id)
	f = open(fname, "w")
	f.close()

	# add desktop to DB
	fname = "%s/_clarity/db.pkl" % (settings.APP_PATH)
	if not os.path.isfile(fname) :
		# create new DB, as one does not exist
		f = open(fname, "wb")
		dict_iconlist = {}
		dict_iconpos = {}
		dict_resolution = {}
		dict_name = {id: name}

		db = {	"iconlist" :	dict_iconlist,
				"iconpos" : 	dict_iconpos,
				"resolution" : 	dict_resolution,
				"name" : 		dict_name}

		pickle.dump(db, f)
		f.close()			
	else :
		f = open(fname, "rb")
		db = pickle.load(f)
		f.close()

		db["iconlist"][id] = []
		db["iconpos"][id] = []
		db["resolution"][id] = ""
		db["name"][id] = name

		f = open(fname, "wb")
		pickle.dump(db, f)
		f.close()





def desktop_slot_renumber(current_id, new_id, new_name) :

	desktop_slot_delete(current_id)
	desktop_slot_init(new_id, new_name)




def desktop_slot_delete(id) :

	# delete slot folder (deleting all desktop files! WARNING!)
	shell("rm -rf %s/%d" % (settings.APP_PATH, id))

	# delete desktop in DB
	fname = "%s/_clarity/db.pkl" % (settings.APP_PATH)
	if os.path.isfile(fname) :
		f = open(fname, "rb")
		db = pickle.load(f)
		f.close()

		if db["iconlist"].get(id, -1) != -1 : 	del db["iconlist"][id]
		if db["iconpos"].get(id, -1) != -1 : 	del db["iconpos"][id]
		if db["resolution"].get(id, -1) != -1 :	del db["resolution"][id]
		if db["name"].get(id, -1) != -1 :		del db["name"][id]

		f = open(fname, "wb")
		pickle.dump(db, f)
		f.close()	





def desktop_slot_checkin(id) :
	"""
	Stores the current Desktop in slot id
	"""

	# If folder slot for current desktop does not exist for some reason, quickly re-create it
	if not desktop_slot_exists(id) :
		shell("mkdir %s/%d" % (settings.APP_PATH, id))

	# (1) get icon names, positions and current screen resolution (via applescript)
	iconlist, iconpos, resolution = Desktop_get_layout()

	# (2) store icon names, positions and current screen resolution to database
	fname = "%s/_clarity/db.pkl" % (settings.APP_PATH)
	f = open(fname, "rb")
	db = pickle.load(f)
	f.close()

	db["iconlist"][id] = iconlist
	db["iconpos"][id] = iconpos
	db["resolution"][id] = resolution
		# name of desktop was already set, when it was assigned

	f = open(fname, "wb")
	pickle.dump(db, f)
	f.close()

	# (3) Move all files from Desktop/ into slot with id (which is assumed to exist)
	shell("shopt -s dotglob nullglob; mv %s/* %s/%d" % (settings.DESKTOP_PATH, settings.APP_PATH, id))

		# Note: first command is required to move also hidden files/folders
		# https://unix.stackexchange.com/questions/6393/how-do-you-move-all-files-including-hidden-from-one-directory-to-another

	# remove any "checked out" flag in this slot. This slot is not checked out any more.
	fname = "%s/%d/.checkedout" % (settings.APP_PATH, id)
	if os.path.isfile(fname) :
		shell("rm -rf %s" % (fname))





def desktop_slot_checkout(id) :
	"""
	Pulls the desktop out of slot id, to be the current Desktop
	"""

	# get current screen resolution of Desktop
	_, _, resolution_now = Desktop_get_layout()

	# move all files from slot with id, to the current Desktop
	shell("shopt -s dotglob nullglob; mv %s/%d/* %s" % (settings.APP_PATH, id, settings.DESKTOP_PATH))

	# put a "checked out" flag in the slot -- so another computer cannot check out this slot
	fname = "%s/%d/.checkedout" % (settings.APP_PATH, id)
	f = open(fname, "w")
	f.close()

	# get icon names, icon positions, and screen res icons were stored at
	fname = "%s/_clarity/db.pkl" % (settings.APP_PATH)
	f = open(fname, "rb")
	db = pickle.load(f)
	f.close()

	iconlist = db["iconlist"][id]
	iconpos = db["iconpos"][id]
	resolution_saved_at = db["resolution"][id]

	# restore desktop
	Desktop_set_layout(iconlist, iconpos, resolution_saved_at, resolution_now)





def desktop_slot_is_checked_out(id) :
	"""	
	Returns True, if the desktop slot with id is already checked out
	"""

	fname = "%s/%s/.checkedout" % (settings.APP_PATH, id)
	if os.path.isfile(fname) :
		return True

	return False























#
#
# Desktop operations
#
#


def Desktop_get_layout() :
	"""
	For the current desktop, returns icon names, icon positions, and resolution
	(This info is gained via an Applescript call)
	"""
	shell("osascript %s/_clarity/get_iconpos.scpt \"%s/_clarity/iconpos.txt\" &>/dev/null" % (settings.APP_PATH, settings.APP_PATH))

	iconposfile = "%s/_clarity/iconpos.txt" % (settings.APP_PATH)
	with open(iconposfile) as f :  data = f.readline()

	data = data.split("::delimiter::")

	iconlist = []
	for i in data[0].split(":") :
		if i != "" and i != "\n" : iconlist.append(i)	
	
	iconpos = []
	for i in data[1].split(":") :
		if i != "" and i != "\n" : iconpos.append(i)

	resolution = []
	for i in data[2].split(":") :
		if i != "" and i != "\n" : resolution.append(i)	

	# delete iconpos.txt output file
	shell("rm -rf %s/_clarity/iconpos.txt" % (settings.APP_PATH))

	return iconlist, iconpos, resolution





def Desktop_set_layout(iconlist, iconpos, resolution_saved_at, resolution_now) :
	"""
	Moves positions of current desktop items back to their original saved positions
	(via temporary Applescript file)
	"""

	# work out resolution scaling
	iconpos_scaled = resolution_scaling(iconpos, resolution_saved_at, resolution_now)

	fname = "%s/_clarity/set_iconpos.scpt" % (settings.APP_PATH)
	f = open(fname, "w")

	f.write("""tell application "Finder"
	
	set icon_names to (get name of every item of desktop)
	
	repeat with n in icon_names
""")

	for i, n in enumerate(iconlist) :
		# double quotes in names need escaping in the applescript way, 
		# because in the applescript code, double quotes actually enclose the string
		n_quotes_escaped = n.replace('"', '\\"')
		
		e = ""
		if i > 0 : e = "else "		
		f.write("\t\t%sif ((n as string) is \"%s\") then\n" % (e, n_quotes_escaped))
		f.write("\t\t\tset desktop position of item n to {%s}\n" % (iconpos_scaled[i]))
			
	f.write("\t\tend if")

	f.write("""
	end repeat
	
end tell
""")

	f.close()

	# execute then delete applescript file
	shell("osascript %s/_clarity/set_iconpos.scpt &>/dev/null" % (settings.APP_PATH))	
	shell("rm -rf %s/_clarity/set_iconpos.scpt" % (settings.APP_PATH))
	




def Desktop_is_waiting_assignment() :
	"""
	Returns True if the Desktop folder contains visible files, but no .desktopid file

	Other hidden files like .DS_Store and .localized are ignored
	"""
	userfiles = 0
	desktopid_exists = 0
	for f in os.listdir(settings.DESKTOP_PATH) :
		if f == ".desktopid" : 
			desktopid_exists = 1
		elif f[0] != "." :
			userfiles += 1

	return (userfiles > 0) and (desktopid_exists == 0)





def Desktop_is_empty() :
	"""
	Returns true if the Desktop folder contains no visible files, and no hidden .desktopid file
	
	The desktop can still be empty if other hidden files like .DS_Store and .localized are present
	These other hidden files are ignored
	"""

	userfiles = 0
	for f in os.listdir(settings.DESKTOP_PATH) :
		if f[0] == "." :
			# hidden file - normally not counted: the only hidden file counted as .desktopid
			if f == ".desktopid" :
				userfiles += 1
		else :
			userfiles += 1

	return userfiles == 0





def Desktop_get_id() :
	"""
	Returns id of the passed desktop folder
	i.e. reads the hidden .desktopid file
	If no file exists, -1 is returned.
	"""

	fname = "%s/.desktopid" % (settings.DESKTOP_PATH)
	if os.path.isfile(fname) :

		f = open(fname, "r")
		id = f.readline()
		f.close()

		return int(id)

	else :
		return -1




def Desktop_set_id(id) :
	"""
	Sets the id of the Desktop folder, on this machine
	"""
	fname = "%s/.desktopid" % (settings.DESKTOP_PATH)
	f = open(fname, "w")
	f.write(str(id))
	f.close()




# OK
def resolution_scaling(iconpos, resolution_saved_at, resolution_now) :
	"""
	Returns new co-ordinate list for icons, doing resolution scaling between machines
	"""
	
	x0, y0 = resolution_saved_at[0].split(",")
	x0 = int(x0); y0 = int(y0)
	x1, y1 = resolution_now[0].split(",")
	x1 = int(x1); y1 = int(y1)	

	iconpos_scaled = []

	for pos in iconpos :
		x,y = pos.split(",")
		x = int(x); y = int(y)

		x_scaled = int(round((x/x0) * x1))
		y_scaled = int(round((y/y0) * y1))

		iconpos_scaled.append("%d,%d" % (x_scaled, y_scaled))

	return iconpos_scaled
