#!/usr/bin/env python3

#
# Switches between desktops.
# Switching saves the contents and positions of the current Desktop,
# and then re-instates the contents and icon positions of the requested Desktop
#
# Ben Shirt-Ediss, 2018
#

import settings
from common import *


if __name__ == "__main__" :

	num_args, next_id, _ = get_commandline_args(min_id = 0)
	
	# if there are no arguments, just list all current desktops
	if num_args == 0 :
		desktop_slot_list_all()
		quit()

	# check for properly specified number of desktop to switch to
	if next_id == -1 :
		error_msg("(Failed) Specify the Desktop number to switch to (e.g. \"sw 3\").")	
	
	# find the state of the current desktop: this determines how to switch
	current_id = Desktop_get_id()

	if Desktop_is_empty() :
		# ----------------------------------------------------------------------
		# State A: empty desktop (absolutely no files, including no hidden files), no id
		# ----------------------------------------------------------------------
		
		# cannot switch to empty desktop state, since we are already in empty desktop state
		if next_id == 0 :
			error_msg("(Failed) Already on the Empty Desktop.")

		# check desktop being switched to exists
		if not desktop_slot_exists(next_id) :
			error_msg("(Failed) Desktop %d does not exist, so cannot be switched to." % (next_id))		

		# cannot switch, if slot requested is already checked out
		if next_id > 0 : # (you can always switch to empty desktop)
			if desktop_slot_is_checked_out(next_id) :
				error_msg("(Failed) Desktop %d is currently checked out by another computer and cannot be switched to." % (next_id))

		# just check out requested desktop from its slot and replace the empty desktop with it
		desktop_slot_checkout(next_id)
		desktop_slot_list_all()

	elif current_id == -1 :
		# ----------------------------------------------------------------------
		# State B: desktop with files, no id
		# ----------------------------------------------------------------------

		error_msg("(Failed) The current Desktop has files, but is waiting to be assigned a number. \nFirst assign a number to the current Desktop (e.g. \"ad 4\"), and then you can switch away from it.")
	else :
		# ----------------------------------------------------------------------
		# State C: desktop with id (may or may not have files) -- it is tracked in Clarity system
		# ----------------------------------------------------------------------
		
		# cannot switch, if desktop requested is the current one
		if next_id == current_id :
			error_msg("(Failed) Desktop %d is already the current Desktop!" % (next_id))		

		# desktop being switched to must exist, if a desktop other than 0 is being requested
		if next_id == 0 :
			pass # fine
		elif desktop_slot_exists(next_id) :
			pass # fine
		else :
			error_msg("(Failed) Cannot switch to Desktop %d as it does not exist." % (next_id))	

		# cannot switch, if slot requested is checked out on another computer
		if next_id > 0 : # (can always switch to empty desktop)
			if desktop_slot_is_checked_out(next_id) :
				error_msg("(Failed) Desktop %d is currently checked out by another computer and cannot be switched to." % (next_id))
	
		# push current desktop back to its slot
		desktop_slot_checkin(current_id)

		# only pull new desktop out if its slot, if its not desktop 0
		if next_id == 0 :
			pass
		else :
			desktop_slot_checkout(next_id)

		desktop_slot_list_all() 
