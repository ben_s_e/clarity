# Clarity - Multiple Desktops on Mac OS

Clarity is a free open source application for Mac OS that allows the user to effectively have multiple desktops, each with their own file layout, and to switch which one is displayed as the current desktop.

See http://shirt-ediss.me/clarity for installation instructions.

(c)Ben Shirt-Ediss, 2018. All rights reserved.

This software is distributed under the terms of the Creative Commons Attribution-ShareAlike 4.0 International Licence. See https://creativecommons.org/licenses/by-sa/4.0/. The software comes with no warranty.