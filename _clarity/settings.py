#!/usr/bin/env python3

# the full path to the Desktop folder on this computer
DESKTOP_PATH = 		"/Users/ben/Desktop"

# the full path to the clarity app folder on this computer
APP_PATH =	 		"/Users/ben/Dropbox/desktops"