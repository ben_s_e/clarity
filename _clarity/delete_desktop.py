#!/usr/bin/env python3

# Deletes a specific desktop slot.
# This desktop slot cannot be checked out on the current machine (i.e. is the current Desktop),
# or be check out on any other machine.
#
# Ben Shirt-Ediss, 2018
#

import settings
from common import *


if __name__ == "__main__" :

	num_args, del_id, _ = get_commandline_args(min_id = 1)

	# check for properly specified number of desktop to delete
	if num_args < 1 :
		error_msg("(Failed) Supply the Desktop number to delete.")
	if del_id == -1 :
		error_msg("(Failed) Number of Desktop to delete must be 1 or greater.")		

	# cannot delete the specified desktop if its checked out (by any computer, including this computer)
	if desktop_slot_is_checked_out(del_id) :
		error_msg("(Failed) Desktop %d is currently checked out and cannot be deleted." % (del_id))

	# check desktop slot actually exists to delete
	if not desktop_slot_exists(del_id) :
		error_msg("(Failed) Desktop %d does not exist to delete." % (del_id))		

	# confirm with user that they wish to delete this desktop slot
	dname = desktop_slot_get_name(del_id)
	print("")
	print("Are you sure that you want to delete Desktop %d [%s]?" % (del_id, dname))
	print("")
	print("All files on this Desktop will be PERMANENTLY deleted (not put in the Trash).")
	confirm = yes_no_confirm()

	print("")
	if confirm == True :

		desktop_slot_delete(del_id)
		
		print("Desktop %d [%s] deleted." % (del_id, dname))
	else :
		print("Delete cancelled.")
	
	desktop_slot_list_all()



