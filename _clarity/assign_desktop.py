#!/usr/bin/env python3

# Assigns New Desktop a number and an optional name, so it has a desktop slot in the Clarity system.  
# The next time the desktop is switched, the desktop icons and layout will be saved.
#
# Desktop name should be a single word, and can use e.g. hyphens or underscores to separate words, e.g.
#	the-desktop-name
#
# Ben Shirt-Ediss, 2018
#

import settings
from common import *
from string import ascii_letters


if __name__ == "__main__" :

	num_args, new_id, new_name = get_commandline_args(min_id = 1)

	# current desktop can only be assigned a number, if it does not have one already
	current_id = Desktop_get_id()
	if current_id != -1 :
		error_msg("(Failed) Current Desktop is already assigned as Desktop %d. \nTo add a new Desktop, first switch to Desktop 0 (\"sw 0\"), add files, then call \"ad %s %s\" again." % (current_id, new_id, new_name))

	# check for properly specified number of desktop to add
	if num_args < 1 :
		error_msg("(Failed) Supply a number and name (optional) for this Desktop.")
	if new_id == -1 :
		error_msg("(Failed) Desktop must be assigned number 1 or greater.")

	# check if requested desktop number is actually free
	if desktop_slot_exists(new_id) :
		error_msg("(Failed) Desktop %d already exists. Choose a different number." % (new_id))

	# if no name specified, give desktop name of "default"
	if new_name == "" : 
		new_name = "default"
	else :
		# check name only has alphanumeric characters
		if all(c in ascii_letters+"0123456789-_" for c in new_name):	
			pass
		else :
			error_msg("(Failed) Desktop name must only have letters, numbers and - or _ to separate words.")

	# make folder for desktop slot, and add to names dictionary
	desktop_slot_init(new_id, new_name)

	desktop_slot_list_all() 
