#!/usr/bin/env python3

# Gives the current desktop a different number and/or name.
#
# Desktop name should be in the form
#	the-desktop-name
#
# Ben Shirt-Ediss, 2018
#

import settings
from common import *
from string import ascii_letters


if __name__ == "__main__" :

	# current desktop must already have an id, in order to be assigned a new one
	current_id = Desktop_get_id()
	if current_id == -1 :
		error_msg("(Failed) The current Desktop cannot be re-numbered, because it is either the Empty Desktop, or is a New Desktop waiting to be given a number.")

	num_args, new_id, new_name = get_commandline_args(min_id = 1)

	# check for properly specified number of desktop
	if num_args < 1 :
		error_msg("(Failed) Supply the new number and name (optional) to assign to this Desktop (e.g. \"rd 5 new_desktop\").")
	if new_id == -1 :
		error_msg("(Failed) Desktop must be assigned number 1 or greater.")

	# check if requested desktop number is actually free
	# (only check if the desktop id number is actually changing)
	if current_id != new_id :
		if desktop_slot_exists(new_id) :
			error_msg("(Failed) Desktop %d already exists. Choose a different number." % (new_id))

	# if no name specified, give desktop name of "default"
	if new_name == "" : 
		new_name = "default"
	else :
		# check name only has alphanumeric characters
		if all(c in ascii_letters+"0123456789-_" for c in new_name):	
			pass
		else :
			error_msg("(Failed) The new Desktop name must only have letters, numbers and - or _ to separate words.")	

	# Update database, with new desktop number and name (delete old details)
	desktop_slot_renumber(current_id, new_id, new_name)

	desktop_slot_list_all() 
	



